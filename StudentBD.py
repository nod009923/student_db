db_students = {}
last_id = 0


def rewrite_save():
    file = open('students.save', 'w')
    line_break = 0
    for id_del_add in db_students:
        file.write(str(id_del_add) + ',')
        for stud_del_add in db_students[id_del_add]:
            file.write(str(stud_del_add) + ',')
        line_break = line_break + 1
        if line_break >= len(db_students):
            break
        else:
            file.write('\n')
    file.close()


def load_save():
    file = open('students.save', 'r')
    file_text_data = file.read()
    file.close()
    if not file_text_data:
        print('Файл пуст')
    else:
        students_text_data = file_text_data.split('\n')  # Получаем массив студентов в виде строк с их данными.

        for student_data in students_text_data:
            trans_mas = student_data.split(',')
            trans_zero = int(trans_mas[0])
            trans_one = str(trans_mas[1])
            trans_two = int(trans_mas[2])
            trans_three = int(trans_mas[3])

            db_students[trans_zero] = (trans_one, trans_two, trans_three)


def except_input(massage):
    while True:
        try:
            result = int(input(massage))
            break
        except:
            print('НЕКОРРЕКТНЫЙ ВВОД!!!!!!!!!')

    return result


def show_students():
    if len(db_students) == 0:
        print("База данных пуста!")
    else:
        for key in db_students:
            student = db_students[key]
            fio = student[0]
            age = student[1]
            course = student[2]
            print('Id:', key, 'ФИО:', fio, 'Возраст:', age, 'Курс:', course)


def add_students():
    global last_id
    file = open('students.save', 'a')
    fio = str(input('Введите ФИО Студента: '))
    age = except_input('Введите возраст Студента: ')
    course = except_input('Введите курс Студента: ')
    students_keys = db_students.keys()

    if not db_students:
        last_id = 1
    else:
        last_id = list(students_keys)[-1] + 1

    db_students[last_id] = (fio, age, course)

    if len(db_students) == 1:
        file.write('')
    else:
        file.write('\n')

    file.write(str(last_id) + ',')
    for student in db_students[last_id]:
        file.write(str(student) + ',')
    file.close()
    print()


def delete_student():
    if len(db_students) > 0:
        while True:
            input_id_student = except_input("Введите id студента: ")
            try:
                db_students.pop(input_id_student)
                rewrite_save()
                break
            except KeyError:
                print('Таких тута нету')
    else:
        print('В базе нету студентов\n\n')


def change_student():
    if len(db_students) > 0:
        flag = True
        while flag == True:
            change = except_input('Введите id студента: ')
            try:
                for run_id in db_students:
                    if change == run_id:
                        fio = str(input('Введите ФИО Студента: '))
                        age = except_input('Введите возраст Студента: ')
                        course = except_input('Введите курс Студента: ')
                        db_students[change] = (fio, age, course)
                        rewrite_save()
                        flag = False

                if change not in db_students:
                    print('Такого студента нету!!!!')

            except KeyError:
                print('Таких тута нету')
    else:
        print('В базе нету студентов\n\n')


##############################################################################################################################################################################################################
load_save()
while True:
    a = except_input('1: Просмотр Студентов.\n2: Добавление Студентов.\n3: Удаление Студентов.\n4: Изминение Студента.\n5: Выйти.\n')
    if a == 1:
        show_students()

    elif a == 2:
        add_students()

    elif a == 3:
        delete_student()

    elif a == 4:
        change_student()

    elif a == 5:
        print('ПАКА')
        break

print('хуй')





